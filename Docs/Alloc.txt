
Module name   "BootFX"
*Commands     "BootFX_*"
SysVars       "BootFX$*"

// C:Global.h.SWIs
#define BootFXSWI_Base (0x59140)

// C:Global.h.NewErrors
#define ErrorBase_BootFX (0x81ef00)
